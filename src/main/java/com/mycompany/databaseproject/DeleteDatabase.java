/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.databaseproject;

import java.sql.*;

/**
 *
 * @author punya
 */
public class DeleteDatabase {
     public static void main(String[] args) {
        Connection conn = null;
        String url = "jdbc:sqlite:dcoffee.db";
        // Connect to database
        try {
            conn = DriverManager.getConnection(url);
            System.out.println("Connection to SQLite has been esablish.");
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return;
        }

        // Selection
        
        String sql = "DELETE FROM category WHERE category_id=?";
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, 7);
        
           int status = stmt.executeUpdate();
//          ResultSet key = stmt.getGeneratedKeys();
//          key.next();
//          System.out.println(""+ key.getInt(1));
           
        } catch (SQLException ex) {
             System.out.println(ex.getMessage());
        }

        // Close Database
        if (conn != null) {
            try {
                conn.close();
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
            }
        }
        
    }

}
